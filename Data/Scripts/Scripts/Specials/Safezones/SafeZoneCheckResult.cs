﻿namespace Scripts.Specials.Safezones
{
    public enum SafeZoneCheckResult
    {
        Ok,
        EnemySafeZone,
        FriendSafeZone,
        EnemyPlayer,
        RestrictedArea
    }
}
