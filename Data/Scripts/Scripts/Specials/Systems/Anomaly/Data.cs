﻿using Digi;
using ProtoBuf;
using ServerMod;
using System.Collections.Generic;
using VRage.Game.ModAPI;
using VRageMath;

namespace Scripts.Specials.Systems.Anomaly
{
    public enum AnomalyEffect
    {
        VoxelBuff, VoxelDeBuff, Darkness, Protection,
            

        Voxels = VoxelBuff | VoxelDeBuff
    }


    public class AnomalySettings
    {
        public EffectSettings VoxelBuff;
        public EffectSettings VoxelDeBuff;
        public EffectSettings Darkness;
        public EffectSettings Protection;

       
    }

    public class EffectSettings
    {
        public string Item;
        public int ItemAmount;
        public float Multiplier;
        public float Radius;
        public long Duration;
        public long ProtectionDuration;
    }

    [ProtoContract]
    public class Anomaly
    {
        [ProtoMember(1)]
        public BoundingSphereD Where;
        [ProtoMember(2)]
        public long Untill;
        [ProtoMember(3)]
        public long CantUseAreaUntill;
        [ProtoMember(4)]
        public AnomalyEffect Effect;


        [ProtoMember(10)]
        public float Multiplier = 1f;

        public static void Clear (List<IMyCubeGrid> connected)
        {
            ApplyVoxelEffect (connected, false, 1f);
        }

        public void Apply(List<IMyCubeGrid> connected, bool enabled) { 
            switch (Effect)
            {
                case AnomalyEffect.VoxelBuff:
                case AnomalyEffect.VoxelDeBuff:
                    ApplyVoxelEffect (connected, enabled, Multiplier);
                    break;
            }
        }

        public Color GetColor ()
        {
            switch (Effect)
            {
                case AnomalyEffect.VoxelBuff: return Color.Blue * 0.2f;
                case AnomalyEffect.VoxelDeBuff: return Color.Magenta * 0.2f;
                case AnomalyEffect.Darkness: return Color.Black;
                case AnomalyEffect.Protection: return Color.Gold;
                default: return Color.WhiteSmoke * 0.2f;
            }
        }

        public void Validate (AnomalySettings settings)
        {
            EffectSettings es = null;
            switch (Effect)
            {
                case AnomalyEffect.VoxelBuff:
                    es = settings.VoxelBuff;
                    break;
                case AnomalyEffect.VoxelDeBuff:
                    es = settings.VoxelDeBuff;
                    break;
                case AnomalyEffect.Darkness:
                    es = settings.Darkness;
                    break;
                case AnomalyEffect.Protection:
                    es = settings.Protection;
                    break;
                default: 
                    Log.ChatError ($"Validation failed for Effect type {Effect}");
                    return;
            }


            Where.Radius = es.Radius;
            Untill = SharpUtils.timeStamp() + es.Duration;
            CantUseAreaUntill = SharpUtils.timeStamp() + es.ProtectionDuration;
            Multiplier = es.Multiplier;
        }

        private static void ApplyVoxelEffect (List<IMyCubeGrid> connected, bool enabled, float mlt) {
            foreach (var c in connected)
            {
                var sh = c.GetShip();
                //foreach (var x in sh.Drills)
                //{
                //    x.DrillHarvestMultiplier = enabled ? mlt : 1f;
                //}
            }
        }
    }
}
