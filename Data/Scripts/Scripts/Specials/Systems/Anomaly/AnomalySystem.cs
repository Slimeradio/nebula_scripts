﻿using Digi;
using ProtoBuf;
using Sandbox.Game;
using Sandbox.ModAPI;
using Scripts.Shared;
using Scripts.Specials.Messaging;
using ServerMod;
using Slime;
using System;
using System.Collections.Generic;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.Utils;
using VRageMath;

namespace Scripts.Specials.Systems.Anomaly
{
    static class Sugar
    {
        public static bool IsVoxel(this AnomalyEffect effect)
        {
            return effect == AnomalyEffect.VoxelBuff || effect == AnomalyEffect.VoxelDeBuff;
        }
    }

    //[MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation | MyUpdateOrder.AfterSimulation)]
    public class AnomalySystem : SessionComponentWithSyncSettings<List<Anomaly>>
    {
        public static MyStringId SQUARE = MyStringId.GetOrCompute("Square");
        private static AnomalySystem Instance;
        private static int iter = 0;

        private static AnomalySettings AnomalySettings = new AnomalySettings();

        public static List<Anomaly> Cache = new List<Anomaly>();

        protected override List<Anomaly> GetDefault()
        {
            var l = new List<Anomaly>();
            l.Add(new Anomaly()
            {
                Where = new BoundingSphereD(new Vector3D(158162.86, 231986.00, -252789.76), 3000),
                Effect = AnomalyEffect.VoxelDeBuff,
                Untill = SharpUtils.timeStamp() + 30000,
                CantUseAreaUntill = SharpUtils.timeStamp() + 30000,
                Multiplier = 0.0001f,
            });

            l.Add(new Anomaly()
            {
                Where = new BoundingSphereD(new Vector3D(158592.91, 228509.23, -241909.87), 3000),
                Effect = AnomalyEffect.VoxelBuff,
                Untill = SharpUtils.timeStamp() + 30000,
                CantUseAreaUntill = SharpUtils.timeStamp() + 30000,
                Multiplier = 5f,
            });

            return l;
        }

        protected AnomalySettings GetDefaultSettings ()
        {
            return new AnomalySettings
            {
                Darkness = new EffectSettings
                {
                    Item = "Component/AnomalyDarkness",
                    ItemAmount = 1,
                    Duration = 3600 * 12,
                    ProtectionDuration = 3600 * 24 * 3,
                    Radius = 5000
                },
                Protection = new EffectSettings
                {
                    Item = "Component/AnomalyProtection",
                    ItemAmount = 1,
                    Duration = 3600 * 12,
                    ProtectionDuration = 3600 * 24 * 3,
                    Radius = 5000
                },
                VoxelBuff = new EffectSettings
                {
                    Item = "Component/AnomalyVoxelBuff",
                    ItemAmount = 1,
                    Duration = 3600 * 12,
                    ProtectionDuration = 3600 * 24 * 3,
                    Radius = 5000
                },
                VoxelDeBuff = new EffectSettings
                {
                    Item = "Component/AnomalyProtection",
                    ItemAmount = 1,
                    Duration = 3600 * 24 * 2,
                    ProtectionDuration = 3600 * 24 * 9,
                    Radius = 6000
                },
            };
        }

        protected override string GetFileName() { return "Anomalies"; }
        protected override ushort GetPort() { return 48829; }

        protected override void HandleData(List<Anomaly> data, byte action, ulong player, bool isFromServer)
        {
            if (isFromServer)
            {
                Settings = data;
            } else
            {
                var newEffect = data[0];
                
                var ts = SharpUtils.timeStamp();
               




                foreach (var oldEffect in Settings)
                {
                    if (oldEffect.Where.Intersects(newEffect.Where))
                    {
                        if (ts >= oldEffect.CantUseAreaUntill) continue;

                        if (oldEffect.Effect == AnomalyEffect.Protection && ts < oldEffect.Untill)
                        {
                            Common.SendChatMessage("You can't use here yet. Here is protection anomaly");
                            return;
                        }

                        
                        if (oldEffect.Effect == newEffect.Effect && ts < oldEffect.CantUseAreaUntill || ts < oldEffect.Untill && (oldEffect.Effect.IsVoxel() && newEffect.Effect.IsVoxel()))
                        {
                            Common.SendChatMessage("You can't use here yet. Overlaping with existing zone");
                            return;
                        }

                        //if (ts < oldEffect)
                        //{
                        //
                        //}

                        if (ts > oldEffect.CantUseAreaUntill)
                        {
                            Common.SendChatMessage("You can't use here yet. Overlaping with existing zone");
                            return;
                        }

                        Common.SendChatMessage ("You can't use here yet. Overlaping with existing zone");
                        return;
                    }
                }


                newEffect.Validate(AnomalySettings);

                Settings.Add (newEffect);
                Sync.SendMessageToOthers(Settings);
            }
        }


        double degree = 0;

        public override void Draw()
        {
            base.Draw();
            if (Settings == null) return;

            try
            {
                degree += Math.PI / 60 / 30;
                var v = new Vector3D (0, Math.Cos(degree), Math.Sin(degree));//);

                var ts = SharpUtils.timeStamp();
                foreach (var x in Settings)
                {
                    var d = (MyAPIGateway.Session.Camera.Position - x.Where.Center).Length();

                    var detail = 12;
                    if (d >= 30000) continue;
                    if (d < 10000) detail = 24;
                    if (d < 6000) detail = 36;

                    var up = Vector3D.Up;
                    var pl = GameBase.GetClosestPlanet(x.Where.Center);
                    if (pl != null)
                    {
                        up = pl.WorldMatrix.Translation - x.Where.Center;
                        up.Normalize();
                    }

                    var c = x.GetColor();
                    var zzz3 = MatrixD.CreateWorld(x.Where.Center, v, Vector3D.Up);
                    MySimpleObjectDraw.DrawTransparentSphere(ref zzz3, (float)x.Where.Radius, ref c, MySimpleObjectRasterizer.Solid, detail, SQUARE);
                }
            } catch (Exception e)
            {
                Log.ChatError (e);
            }
            
            
        }


        public override void Init(MyObjectBuilder_SessionComponent sessionComponent)
        {
            base.Init(sessionComponent);
            Instance = this;
        }

        public override void UpdateBeforeSimulation()
        {
            base.UpdateBeforeSimulation();
            iter++;
        }

        internal static void Logic(Ship from, List<IMyCubeGrid> connected)
        {
            if (true) return;

            if (from.grid.EntityId % 300 == iter % 300) return;
            

            Cache.Clear();
            Instance.GetEnabledInArea (connected, Cache);

            Anomaly.Clear(connected);
            foreach (var a in Cache)
            {
                a.Apply (connected, true);
            }
        }

        public void GetEnabledInArea(List<IMyCubeGrid> connected, List<Anomaly> anomalies)
        {
            if (Settings == null) return;

            var ts = SharpUtils.timeStamp();

            var aabb = connected.GetAABB();
            foreach (var x in Settings)
            {
                if (x.Where.Intersects(aabb) && ts < x.Untill)
                {
                    anomalies.Add(x);
                }
            }
        }
    }
}
